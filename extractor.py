import pandas as pd
import re
import numpy as np
from w2v import Word2Vector
from random import shuffle
from tqdm import tqdm


class Extractor:

    SPLIT_REGEX = re.compile(r"[\w']+")
    NUMBER_REPLACE_REGEX = re.compile(r"[\d]")

    def __init__(self, w2v_path, train_data_path, context_size, query_size, vec_size, batch_size=50):
        self.CONTEXT_SIZE = context_size
        self.QUERY_SIZE = query_size
        self.VEC_SIZE = vec_size
        self.BATCH_SIZE = batch_size
        self.TRAIN_DATA_PATH = train_data_path
        self.W2V_PATH = w2v_path
        self.PAGES = 0
        self.LIMIT = 9999999
        self.data = []
        self.listed_data = []

        self.extract()

    def pages(self):
        shuffle(self.listed_data)
        return int(len(self.listed_data))

    def extract(self):
        all_lines = pd.read_csv(self.TRAIN_DATA_PATH, header=None, encoding='utf-8').to_dict(orient='records')
        drop_by_context_length = 0
        drop_by_query_length = 0
        with Word2Vector(self.W2V_PATH, self.VEC_SIZE) as w2v:
            for record in tqdm(all_lines[1:self.LIMIT], desc='Extracting data set'):
                obj = {}
                context_words = re.findall(self.SPLIT_REGEX, record[2])
                query_words = re.findall(self.SPLIT_REGEX, record[3])
                answer_words = re.findall(self.SPLIT_REGEX, record[4])
                if len(answer_words) == 0:
                    continue
                if len(context_words) > self.CONTEXT_SIZE:
                    drop_by_context_length += 1
                    continue
                if len(query_words) > self.QUERY_SIZE:
                    drop_by_query_length += 1
                    continue
                obj['paragraph_id'] = int(record[1])
                obj['context'] = w2v.convert_to(list(map(lambda x: re.sub(self.NUMBER_REPLACE_REGEX, '0', x), context_words[:self.CONTEXT_SIZE])),
                                   self.CONTEXT_SIZE)
                obj['query'] = w2v.convert_to(list(map(lambda x: re.sub(self.NUMBER_REPLACE_REGEX, '0', x), query_words[:self.QUERY_SIZE])),
                                   self.QUERY_SIZE)
                obj['context_len'] = len(context_words)
                obj['query_len'] = len(query_words)

                start, end = self.define_range(re.findall(self.SPLIT_REGEX, record[4]), context_words[:self.CONTEXT_SIZE])
                if end == 0:
                    continue
                start, end = self.range_to_vector(start, end)
                # obj['answer'] = answer
                obj['start'] = start
                obj['end'] = end
                self.data.append(obj)
            self.prepare_batches(int(len(self.data)/self.BATCH_SIZE))

    def prepare_batches(self, pages):
        for page in tqdm(range(pages), desc='Preparing:'):
            batch_elements = self._batch(page)
            context = list(map(lambda x: x['context'], batch_elements))
            query = list(map(lambda x: x['query'], batch_elements))
            context_len = list(map(lambda x: x['context_len'], batch_elements))
            query_len = list(map(lambda x: x['query_len'], batch_elements))
            start = list(map(lambda x: x['start'], batch_elements))
            end = list(map(lambda x: x['end'], batch_elements))
            tf_feed_dict = {
                'context': np.array(context),
                'query': np.array(query),
                'start': np.array(start),
                'end': np.array(end),
                'context_len': np.array(context_len, dtype=np.int32),
                'query_len': np.array(query_len, dtype=np.int32)
            }
            self.listed_data.append(tf_feed_dict)

    def _batch(self, page):
        start = page*self.BATCH_SIZE
        end = start + self.BATCH_SIZE
        return self.data[start:end]

    def batch(self, page):
        return self.listed_data[page]

    def batches(self):
        for page in range(self.pages()):
            yield self.batch(page)

    def define_range(self, of_words, in_words):
        pattern = ' '.join(of_words)
        text_range = len(of_words)
        index_from = -1
        index_to = 0
        while index_to < len(in_words):
            try:
                index_from += 1
                index_to = index_from + text_range
                if in_words[index_from] != of_words[0]:
                    continue
                elif ' '.join(in_words[index_from:index_to]) == pattern:
                    return index_from, index_to
            except IndexError as error:
                print(error)
                break
        return 0, 0

    def range_to_vector(self, start, end):
        if end >= self.CONTEXT_SIZE:
            return [0]*self.CONTEXT_SIZE
        else:
            vec_start = [0]*self.CONTEXT_SIZE
            vec_end = [0]*self.CONTEXT_SIZE
            vec_start[start] = 1
            vec_end[end] = 1
            return vec_start, vec_end
