class Alphabet:

    TEXT_MAX_LEN = 100

    source = ['а', 'б', 'в',
              'г', 'д', 'е',
              'ё', 'ж', 'з',
              'и', 'й', 'к',
              'л', 'м', 'н',
              'о', 'п', 'р',
              'с', 'т', 'у',
              'ф', 'х', 'ц',
              'ч', 'ш', 'щ',
              'ъ', 'ы', 'ь',
              'э', 'ю', 'я',
              'А', 'Б', 'В',
              'Г', 'Д', 'Е',
              'Ё', 'Ж', 'З',
              'И', 'Й', 'К',
              'Л', 'М', 'Н',
              'О', 'П', 'Р',
              'С', 'Т', 'У',
              'Ф', 'Х', 'Ц',
              'Ч', 'Ш', 'Щ',
              'Ъ', 'Ы', 'Ь',
              'Э', 'Ю', 'Я',
              ' ', ',', '.']

    def __init__(self, max_len):
        self.TEXT_MAX_LEN = max_len

    @property
    def empty_representation(self):
        return [0]*len(self.source)

    def symbol_representation(self, symbol):
        array = [0]*len(self.source)
        try:
            index = self.source.index(symbol)
        except ValueError as error:
            print(error)
            return self.empty_representation

        if len(array) >= index + 1:
            array[index] = 1
        return array

    def one_hot(self, text):
        output = []
        for char in text:
            output.append(self.symbol_representation(char))
        remaining = self.TEXT_MAX_LEN - len(output)
        while remaining > 0:
            output.append(self.empty_representation)
            remaining -= 1
        return output
