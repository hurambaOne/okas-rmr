import tensorflow as tf


class NBCBuilder:

    def __init__(self, context_size, query_size, word_size):
        context = tf.placeholder(tf.float32, [None, context_size, word_size], 'context')
        # query = tf.placeholder(tf.float32, [None, query_size, word_size], 'query')
        w = tf.get_variable()
        b = tf.get_variable()

        cell_fw = tf.nn.rnn_cell.GRUCell(32, reuse=tf.AUTO_REUSE)
        cell_bw = tf.nn.rnn_cell.GRUCell(32, reuse=tf.AUTO_REUSE)

        hidden_context, _ = tf.nn.bidirectional_dynamic_rnn(cell_fw, cell_bw, context, dtype=tf.float32)
        # hidden_query, _ = tf.nn.bidirectional_dynamic_rnn(cell_fw, cell_bw, context, dtype=tf.float32)

        hw = tf.einsum('', hidden_context, w) + b

