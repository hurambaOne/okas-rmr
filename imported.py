import pandas as pd
import re

class BatchModel:

    text = []
    question = []
    answer = []

    def __init__(self, t, q, a):
        self.text = t
        self.question = q
        self.answer = a

    def items(self):
        return


class Import:

    WORD_REG = re.compile(r"[\w']+]")
    SENTENCE_REG = re.compile(r"[\w]+|[\.,\s:—]")
    DECIMAL_REG = re.compile(r"[\d]")
    _lines = []

    def __init__(self, path, batch_size):
        self.BATCH_SIZE = batch_size
        self._lines = pd\
            .read_csv(
                path,
                header=None,
                encoding='utf-8')\
            .to_dict(orient='records')

    @property
    def pages(self):
        return int(self._lines/self.BATCH_SIZE)

    def extract(self, batch):
        start_index = batch*self.BATCH_SIZE + 1
        end_index = start_index + self.BATCH_SIZE
        for line in self._lines[start_index:end_index]:
            return BatchModel(line[2], line[3], line[4])