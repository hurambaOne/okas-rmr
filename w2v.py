import os


class Word2Vector:

    dictionary = {}
    PATH = ''
    MAX_TEXT = 0
    MAX_QUERY = 0
    LEN = 50

    def __init__(self, dictionary_path, word_len=50):
        self.file = open(dictionary_path, encoding='utf-8')
        self.LEN = word_len
        self.PATH = dictionary_path

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.dictionary = {}


    def convert_to(self, array, size):
        matrix = []
        for item in array:
            matrix.append(self.get(item))
        remaining = size - len(array)
        while remaining > 0:
            matrix.append(self.empty_item)
            remaining -= 1
        return matrix

    @property
    def empty_item(self):
        return [.0]*self.LEN

    def get(self, word):
        if word.lower() in self.dictionary:
            return self.dictionary[word.lower()]
        else:
            while True:
                try:
                    source = self.file.readline()
                    if source is None or source == '':
                        return [.0]*self.LEN
                    values = source.split(';')
                    if len(values) < 2:
                        continue
                    key = values[0]
                    value = list(map(float, values[1:self.LEN+1]))
                    if key == '</s>':
                        key = ' '
                    self.dictionary[key] = value
                    if key == word.lower():
                        return value
                except EOFError as error:
                    print(error)
                    return [.0]*self.LEN
