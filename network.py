from rmr import RMRNetwork
import tensorflow as tf
from extractor import Extractor
import numpy as np
from tqdm import tqdm
from logger import Logger

if __name__ == '__main__':
    context_size = 400
    query_size = 50
    wordlength = 50
    cell_size = 32
    batch_size = 120
    epochs = 100
    interaction_steps = 1
    answer_arrangment_steps = 1

    rmr = RMRNetwork(context_size, query_size, wordlength, cell_size)
    context, query = rmr.build_encode_layer()
    print('encoding layer is ready...')
    # fully_aware_context = rmr.build_interaction_align_layer(context, query, interaction_steps)
    # print('alignment layer is ready...')
    rmr.full_aware_answer(context, query)
    # rmr.build_answer_layer(context, query, answer_arrangment_steps)
    print('answer layer is ready...')
    train_op, loss = rmr.build_loss_layer()
    print('train layer is ready')
    extractor_train = Extractor('./data/w2v.txt', './data/train.csv', context_size, query_size, wordlength, batch_size)
    extractor_test = Extractor('./data/w2v.txt', './data/test.csv', context_size, query_size, wordlength, batch_size)
    with tf.Session() as session:
        init = tf.global_variables_initializer()
        session.run(init)
        writer = tf.summary.FileWriter("output", session.graph)

        for epoch in range(epochs):
            pages = extractor_train.pages()
            pages_test = extractor_test.pages()

            s_index_match = 0
            s_exact_match = 0
            s_f_score = 0
            s_loss = 0

            for page in tqdm(range(pages), desc='Epoch #' + str(epoch+1)):
                batch = extractor_train.batch(page)
                try:
                    _, loss, probs, index_match, exact_match, f_score = rmr.train(session, {
                        'context:0': batch['context'],
                        'query:0': batch['query'],
                        'start:0': batch['start'],
                        'end:0': batch['end'],
                        'context_len:0': batch['context_len'],
                        'query_len:0': batch['query_len']
                    })
                except ValueError as error:
                    raise error

                s_index_match += index_match
                s_exact_match += exact_match
                s_loss += loss
                s_f_score += f_score

            print('================Train================')
            print('Examples: ' + str(pages*batch_size))
            print('Index Matches: ' + str(s_index_match/(pages*batch_size)))
            print('Exact Matches: ' + str(s_exact_match/(pages*batch_size)))
            print('F1 score: ' + str(s_f_score/pages))
            print('Losses: ' + str(s_loss/(pages*batch_size)))

            Logger().put([
                ('Epoch', epoch),
                ('Stage', 'Train'),
                ('Index Matches', str(s_index_match/(pages*batch_size))),
                ('F1 score', str(s_f_score/pages)),
                ('Losses', str(s_loss/(pages*batch_size)))
            ])

            s_index_match = 0
            s_exact_match = 0
            s_f_score = 0
            s_loss = 0

            for page in tqdm(range(pages_test), desc='Epoch_test #' + str(epoch)):
                batch = extractor_test.batch(page)
                try:
                    loss, probs, index_match, exact_match, f_score = rmr.test(session, {
                        'context:0': batch['context'],
                        'query:0': batch['query'],
                        'start:0': batch['start'],
                        'end:0': batch['end'],
                        'context_len:0': batch['context_len'],
                        'query_len:0': batch['query_len']
                    })
                except ValueError as error:
                    raise error

                s_index_match += index_match
                s_exact_match += exact_match
                s_loss += loss
                s_f_score += f_score

            print('================Test================')
            print('Examples: ' + str(pages_test * batch_size))
            print('Index Matches: ' + str(s_index_match / (pages_test * batch_size)))
            print('Exact Matches: ' + str(s_exact_match / (pages_test * batch_size)))
            print('F1 score: ' + str(s_f_score / pages_test))
            print('Losses: ' + str(s_loss / (pages_test * batch_size)))

            Logger().put([
                ('Epoch', epoch),
                ('Stage', 'Test'),
                ('Index Matches', str(s_index_match / (pages_test * batch_size))),
                ('F1 score', str(s_f_score / pages_test)),
                ('Losses', str(s_loss / (pages_test * batch_size)))
            ])



