import tensorflow as tf


class RMRNetwork:

    ADAM_OPTIMIZER = tf.train.AdamOptimizer()
    EPSILON = 0.0000000001
    TRAIN_SIZE = 300

    def __init__(self, text_len, query_len, word_len=50, cell_input_size=32):
        self.QUERY_WORD_COUNT = query_len
        self.TEXT_WORD_COUNT = text_len
        self.WORD_LEN = word_len
        self.CELL_SIZE = cell_input_size

        # self.TANH_W = tf.get_variable('tanh-w', [text_len*4, text_len])
        # self.SIGM_W = tf.get_variable('sigm-w', [text_len*4, text_len])
        # self.TANH_B = tf.get_variable('tanh-b', [text_len, cell_input_size*2])
        # self.SIGM_B = tf.get_variable('sigm-b', [text_len, cell_input_size*2])

        # self.tanh_w_ev = tf.get_variable('end_evidence_t', [cell_input_size*2*2, cell_input_size*2], dtype=tf.float32)
        # self.sigm_w_ev = tf.get_variable('end_evidence_s', [cell_input_size*2*2, cell_input_size*2], dtype=tf.float32)

    def build_encode_layer(self):
        context = tf.placeholder(tf.float32, [None, self.TEXT_WORD_COUNT, self.WORD_LEN], 'context')
        query = tf.placeholder(tf.float32, [None, self.QUERY_WORD_COUNT, self.WORD_LEN], 'query')
        context_len = tf.placeholder(tf.int32, [None], 'context_len')
        query_len = tf.placeholder(tf.int32, [None], 'query_len')

        gru_fw = tf.nn.rnn_cell.GRUCell(self.CELL_SIZE, name='fw', reuse=tf.AUTO_REUSE)
        gru_bw = tf.nn.rnn_cell.GRUCell(self.CELL_SIZE, name='bw', reuse=tf.AUTO_REUSE)
        gru_fq = tf.nn.rnn_cell.GRUCell(self.CELL_SIZE, name='fw_q', reuse=tf.AUTO_REUSE)
        gru_bq = tf.nn.rnn_cell.GRUCell(self.CELL_SIZE, name='bw_q', reuse=tf.AUTO_REUSE)

        hidden_context, states_c = tf.nn.bidirectional_dynamic_rnn(
            cell_fw=gru_fw,
            cell_bw=gru_bw,
            inputs=context,
            sequence_length=context_len,
            dtype=tf.float32)
        hidden_query, states_q = tf.nn.bidirectional_dynamic_rnn(
            cell_fw=gru_fq,
            cell_bw=gru_bq,
            inputs=query,
            sequence_length=query_len,
            dtype=tf.float32)

        hc = tf.concat(hidden_context, 2)
        hq = tf.concat(hidden_query, 2)

        return hc, hq

    def build_interaction_align_layer(self, h_context, h_query, hops=5):
        ct = h_context
        qt = h_query
        # attention = tf.nn.softmax(tf.einsum('ijk,ink->ijn', ct, qt), axis=1)

        while hops > 0:
            hops -= 1

            bt = tf.einsum('ijk,ink->ijn', ct, qt)
            qt = tf.nn.softmax(tf.einsum('ijk,ikn->ijn',bt, qt))
            ct = self.sfu(ct, qt)
            context_aware = tf.matmul(ct, ct, transpose_b=True)
            context_aware = tf.nn.softmax(context_aware, 1)
            cta = tf.matmul(context_aware, ct)
            ct = self.sfu(ct, cta)

            gru_fw_ca = tf.nn.rnn_cell.GRUCell(self.CELL_SIZE, name='fw_context_aware', reuse=tf.AUTO_REUSE)
            gru_bw_ca = tf.nn.rnn_cell.GRUCell(self.CELL_SIZE, name='bw_context_aware', reuse=tf.AUTO_REUSE)

            hidden_context_aware, states_aware = tf.nn.bidirectional_dynamic_rnn(gru_fw_ca, gru_bw_ca, ct, dtype=tf.float32)

            ct = tf.concat(hidden_context_aware, 2)

        return ct

    def _sfu(self, axis=2, *vectors):
        # w_tanh = tf.get_variable('w_tanh_sfu', [self.TEXT_WORD_COUNT*len(vectors), self.CELL_SIZE])
        # w_sigm = tf.get_variable('w_sigm_sfu', [])
        #
        # concat_product = tf.concat(vectors, axis=2)
        # mark_tanh = tf.tanh()
        pass

    def sfu(self, context_vector, query_aware):
        pass
        # dot_product = tf.multiply(context_vector, query_aware)
        # subtrain_cq = tf.subtract(context_vector, query_aware)
        # cross_product = tf.concat([context_vector, query_aware, dot_product, subtrain_cq], 1)
        # tanh_mark = tf.tanh(tf.einsum('ijk,jl->ilk', cross_product, self.TANH_W) + self.TANH_B)
        # sigmoid_mark = tf.sigmoid(tf.einsum('ijk,jl->ilk', cross_product, self.SIGM_W) + self.SIGM_B)

        # output = tf.multiply(tanh_mark, sigmoid_mark) + tf.multiply((1 - sigmoid_mark), context_vector)

        # return output

    def sfu_answer(self, evidence, probability):
        concat_vector = tf.concat([evidence, probability], axis=1)
        tanh_mark = tf.tanh(tf.einsum('ij,jk->ik', concat_vector, self.tanh_w_ev))
        sigmoid_mark = tf.sigmoid(tf.einsum('ij,jk->ik', concat_vector, self.sigm_w_ev))

        output = tf.multiply(tanh_mark, sigmoid_mark) + tf.multiply((1 - sigmoid_mark), evidence)
        return output

    def full_aware_answer(self, context, query):
        att_query_weights = tf.get_variable('att_query_configuration', [self.CELL_SIZE*6])
        ed_context = tf.expand_dims(context, 1)  # ?, 1, 400, 64
        ed_query = tf.expand_dims(query, 2)  # ?, 150, 1, 64
        ed_mul = ed_context*ed_query  # ?, 150, 400, 64

        ed_context = tf.ones(
            shape=[self.QUERY_WORD_COUNT, 1, 1],
            dtype=tf.float32) * ed_context
        ed_query = tf.ones(
            shape=[1, self.TEXT_WORD_COUNT, 1],
            dtype=tf.float32) * ed_query

        ed_concat = tf.concat(
            [ed_context, ed_query, ed_mul],
            axis=3)  # ?, 150, 400, 64*3

        sparse = tf.einsum('ijkn,n->ijk', ed_concat, att_query_weights)

        c2q = tf.einsum('ijk,ijn->ikn', tf.nn.softmax(sparse), query)  # ?, 400, 64

        b = tf.nn.softmax(tf.reduce_max(sparse, axis=1))
        h_attended = tf.einsum('ijk,ij->ik', context, b)
        q2c = context*tf.expand_dims(h_attended, axis=1)

        m_full_attnd = tf.concat([context, c2q, tf.multiply(context, c2q), tf.multiply(context, q2c)], axis=2)  # ?, 420, 384

        gru_a_fw = tf.nn.rnn_cell.GRUCell(self.CELL_SIZE, name='fw_a', reuse=tf.AUTO_REUSE)
        gru_a_bw = tf.nn.rnn_cell.GRUCell(self.CELL_SIZE, name='bw_a', reuse=tf.AUTO_REUSE)

        (hrw, hbw), _ = tf.nn.bidirectional_dynamic_rnn(gru_a_fw, gru_a_bw, m_full_attnd, dtype=tf.float32)

        attended_context = tf.concat([hrw, hbw], axis=2)  # M matrix 400x64

        configuration_end = tf.get_variable('config_end', [self.CELL_SIZE*10, self.TRAIN_SIZE])
        configuration_start = tf.get_variable('config_start', [self.CELL_SIZE*10, self.TRAIN_SIZE])
        ws = tf.get_variable('w_in_trainable', [self.TRAIN_SIZE], dtype=tf.float32)
        we = tf.get_variable('w_out_trainable', [self.TRAIN_SIZE], dtype=tf.float32)
        bs = tf.get_variable('b_in_trainable', [self.TEXT_WORD_COUNT], dtype=tf.float32)
        be = tf.get_variable('b_out_trainable', [self.TEXT_WORD_COUNT], dtype=tf.float32)

        # aware_layer = tf.einsum('ijk,ink->ijn', context, query)

        sc_layer = tf.einsum('ijk,kn->ijn', tf.concat([m_full_attnd, attended_context], axis=2), configuration_start)
        self.prob_start = tf.nn.softmax(tf.einsum('ijk,k->ij', sc_layer, ws) + bs)
        # argmax = tf.argmax(self.prob_start, axis=1)

        end_cell_fw = tf.nn.rnn_cell.GRUCell(self.CELL_SIZE)
        end_cell_bw = tf.nn.rnn_cell.GRUCell(self.CELL_SIZE)

        (transit_ec_f, transit_ec_b), _ = tf.nn.bidirectional_dynamic_rnn(end_cell_fw, end_cell_bw, attended_context, dtype=tf.float32)
        ec_layer = tf.einsum('ijk,kn->ijn', tf.concat([m_full_attnd, transit_ec_f, transit_ec_b], axis=2), configuration_end)

        self.prob_end = tf.nn.softmax(tf.einsum('ijk,k->ij', ec_layer, we) + be)
        self.prob_vector = tf.add(self.prob_start, self.prob_end)

        # self.prob_vector = tf.concat([prob_start, prob_end], 1)

    # query is last hidden state, context is interaction align layer output
    def build_answer_layer(self, context, query, hops=1):
        # prepare memory vector
        z_modifier = tf.ones([self.TEXT_WORD_COUNT], dtype=tf.float32)
        z_start_source = tf.einsum('ijk->ik', query)

        # prepare trainable variables
        ws = tf.get_variable('w_in_trainable', [self.TEXT_WORD_COUNT*3, self.TEXT_WORD_COUNT], dtype=tf.float32)
        we = tf.get_variable('w_out_trainable', [self.TEXT_WORD_COUNT*3, self.TEXT_WORD_COUNT], dtype=tf.float32)
        bs = tf.get_variable('b_in_trainable', [self.TEXT_WORD_COUNT, self.CELL_SIZE*2], dtype=tf.float32)
        be = tf.get_variable('b_out_trainable', [self.TEXT_WORD_COUNT, self.CELL_SIZE*2], dtype=tf.float32)
        hw_start = tf.get_variable('hw_start', [self.CELL_SIZE*2, 1], dtype=tf.float32)
        hw_end = tf.get_variable('hw_end', [self.CELL_SIZE*2, 1], dtype=tf.float32)

        while hops > 0:
            z_start = tf.einsum('ij,n->inj', z_start_source, z_modifier)
            cz_mul = tf.multiply(context, z_start)
            cc_start = tf.concat([context, z_start, cz_mul], axis=1)  # 1200 x 64
            s_start = tf.einsum('ijk,jn->ink', cc_start, ws) + bs  # 400x64

            prob_start = tf.nn.softmax(tf.einsum('ijk,kn->ij', s_start, hw_start))  # 400x1
            u_start = tf.einsum('ijk,ij->ik', context, prob_start)
            z_end_source = self.sfu_answer(z_start_source, u_start)

            z_end = tf.einsum('ij,n->inj', z_end_source, z_modifier)
            cz_mul_e = tf.multiply(context, z_end)
            cc_end = tf.concat([context, z_end, cz_mul_e], axis=1)  # 1200 x 64
            s_end = tf.einsum('ijk,jn->ink', cc_end, we) + be  # 400x64

            prob_end = tf.nn.softmax(tf.einsum('ijk,kn->ij', s_end, hw_end))  # 400x1
            u_end = tf.einsum('ijk,ij->ik', context, prob_end)
            z_start_source = self.sfu_answer(z_end_source, u_end)

            hops -= 1
            if hops == 0:
                self.prob_start = prob_start
                self.prob_end = prob_end
                self.prob_vector = tf.concat([prob_start, prob_end], 1)

    def build_loss_layer(self):
        start = tf.placeholder(tf.float32, [None, self.TEXT_WORD_COUNT], name='start')
        end = tf.placeholder(tf.float32, [None, self.TEXT_WORD_COUNT], name='end')
        answer = tf.add(start, end)

        # self.prob = tf.reduce_sum(answer*self.prob_vector)
        self.loss_1 = -tf.reduce_sum(start*tf.log(self.prob_start + self.EPSILON) + (1-start)*tf.log(1-self.prob_start + self.EPSILON))
        self.loss_2 = -tf.reduce_sum(end*tf.log(self.prob_end + self.EPSILON) + (1-end)*tf.log(1-self.prob_end + self.EPSILON))
        # self.loss = tf.losses.log_loss(start, self.prob_start) + tf.losses.log_loss(end, self.prob_end)
        # self.loss = tf.reduce_sum(1 - tf.multiply(answer, self.prob_vector))
        # self.loss = tf.reduce_sum(tf.nn.softmax_cross_entropy_with_logits_v2(labels=answer, logits=self.prob_vector))
        self.train_op = tf.train.AdamOptimizer().minimize(self.loss_1 + self.loss_2)
        self.loss = self.loss_1 + self.loss_2

        start_is_right = tf.equal(tf.argmax(start, 1), tf.argmax(self.prob_start, 1))
        end_is_right = tf.equal(tf.argmax(end, 1), tf.argmax(self.prob_end, 1))
        result = tf.round(self.prob_vector)
        not_answer = 1-answer
        not_result = 1-result
        tp = tf.reduce_sum(answer*result)
        fp = tf.reduce_sum(not_answer*result)
        fn = tf.reduce_sum(answer*not_result)
        pres = tp/(tp+fp+self.EPSILON)
        recall = tp/(tp+fn+self.EPSILON)

        # SCORES

        self.f1_score = 2*(pres*recall)/(pres+recall+self.EPSILON)
        self.true_both = tf.reduce_sum(tf.cast(start_is_right, tf.float32)*tf.cast(end_is_right, tf.float32))
        self.exact_match = tf.reduce_sum(tf.cast(tf.reduce_all(tf.equal(answer, tf.round(self.prob_vector))), dtype=tf.float32))
        # self.accuracy = tf.metrics.accuracy(answer, self.prob_vector)

        return self.train_op, self.loss

    def train(self, session, data):
        return session.run([
            self.train_op,
            self.loss,
            self.prob_vector,
            self.true_both,
            self.exact_match,
            self.f1_score], feed_dict=data)

    def test(self, session, data):
        return session.run([
            self.loss,
            self.prob_vector,
            self.true_both,
            self.exact_match,
            self.f1_score], feed_dict=data)
