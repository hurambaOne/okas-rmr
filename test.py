
arr = [
    {
        'elements': [1,2,3]
    },{
        'elements': [2,3,4]
    },{
        'elements': [3,4,5]
    },{
        'elements': [4,5,6]
    }
]

print(list(map(lambda x: x['elements'], arr)))