from datetime import date


class Logger:

    def put(self, inputs):
        dt = date.today()
        with open('./logs/' + dt.strftime("%d-%m-%y-server"), mode='a+') as file:
            for item in inputs:
                file.write(item[0] + ': ' + str(item[1]) + '\n')
            file.write('==============================\n')

    def text(self, start, end):
        dt = date.today()
        with open('./logs/' + dt.strftime("%d-%m-%y-by-step"), mode='a+') as file:
            file.writelines(
                'Start: ' + str(start) + '\n'
                'End: ' + str(end) +'\n'
                '=============================\n'
            )

    def match(self, value):
        dt = date.today()
        with open('./logs/' + dt.strftime("%d-%m-%y-matching"), mode='w+') as file:
            file.writelines(
                'Exact Match: ' + str(value) + '\n'
            )

    def log(self, train_acc, test_acc, train_loss, test_loss):
        dt = date.today()
        with open('./logs/' + dt.strftime("%d-%m-%y"), mode='a+') as file:
            file.writelines(
                '><><><><><><><><><><><><><><><><><><><><><\n'
                '==========================================\n'
                'Train:\n'
                '>>>>>Avg loss: {0}\n'
                '>>>>>Accuracy: {1}\n'
                'Test:\n'
                '>>>>>Avg loss: {2}\n'
                '>>>>>Accuracy: {3}\n'
                '==========================================\n'.format(train_loss, train_acc, test_loss, test_acc))